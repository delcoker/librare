//#include <string>
#include "Book.h"

Book::Book():MediaItem()
{
	//this->m_Authour = "No Authour";
	//this->setAuthour("No Authour");
	this->m_NumPages = 0;
	this->m_Text = "No Text";
	//this->m_Enabled = true;

	cout << "default constructor called book" << endl;
}

Book::Book(string authour, string title, string mediaType, string shelfmark, bool available, string datePublished, int numPages, bool deleted, string text) : MediaItem(authour, title, mediaType, shelfmark, available, datePublished, deleted)
{
	//this->setAuthour(authour);  // have to explicity set authour to get 
	this->m_NumPages = numPages;
	this->m_Text = text;
}

Book::~Book(){
}


string Book::getAuthour() {
	return this->getCreator();
}

void Book::setAuthour(string authour) {
	this->setCreator(authour);
}

int Book::getNumPages() {
	return m_NumPages;
}

void Book::setNumPages(int numPages) {
	m_NumPages = numPages;
}

string Book::getText()
{
	return this->m_Text;
}

void Book::setText(string text)
{
	this->m_Text = text;
}

string Book::toString() {
	return MediaItem::toString() + "\nAuthour:\t" + getAuthour() +
		"\nNumber of Pages:\t" + to_string(m_NumPages);
}

string Book::toCSV() {
	return MediaItem::toCSV() + "," + getAuthour() +
		"," + to_string(m_NumPages)+ "," + getText();
}



/*
int main() {
	//Book a;		// will be automatically destroyed when out of scope

	Book *a = new Book; // MediaItem();
	string b = "jh";
	a->setNumPages(3);
	(*a).setAuthour("jh");

	cout << a->toString() << endl;

	cin.get();

	delete a;
}


//int var = 8;
//&var : what's the memory address of var

// *var : dereference var (get me the act value of var)

// int* var: create a pointer called var

// const int* a = new int // can NOT change value(contents) at memory address like this *a = 2;
// int* const a = new int // can NOT reassign pointer to somewhere else like this a = (int*)12;

/*
Pass-by-references is more efficient than pass-by-value, because it does not copy the arguments. 
The formal parameter is an alias for the argument. When the called function read or write
the formal parameter, it is actually read or write the argument itself.

The difference between pass-by-reference and pass-by-value is that modifications made to arguments
passed in by reference in the called function have effect in the calling function,
whereas modifications made to arguments passed in by value in the called function can not
affect the calling function. Use pass-by-reference if you want to modify the argument value
in the calling function. Otherwise, use pass-by-value to pass arguments.

The difference between pass-by-reference and pass-by-pointer is that pointers can be NULL or
reassigned whereas references cannot. Use pass-by-pointer if NULL is a valid parameter value or 
if you want to reassign the pointer. Otherwise, use constant or non-constant references to pass arguments.


source: https://www.ibm.com/support/knowledgecenter/en/SSLTBW_2.3.0/com.ibm.zos.v2r3.cbclx01/cplr233.htm
*/