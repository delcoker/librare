#pragma once
#include <iostream>
#include "MediaItem.h"

using namespace std;

class Book : public MediaItem {
private:
	//string m_Authour;
	int m_NumPages;
	string m_Text;

public:
	Book();
	Book(string authour, string title, string mediaType, string shelfmark, bool available, string datePublished, /*string authour,*/ int numPages, bool deleted, string text);
	~Book();
	
	string getAuthour();
	void setAuthour(string authour);

	int getNumPages();
	void setNumPages(int numPages);
	string getText();
	void setText(string text);

	string toString();
	string toCSV();
};
