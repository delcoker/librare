#include "CompactDisk.h"

CompactDisk::CompactDisk():MediaItem()
{
	this->setArtist("No Artist");
	this->m_NumTracks = 0;
	this->capacity = 5;
	this->m_Tracks = new Track[capacity];

	std::cout << "CD: default constructor" << std::endl;
}

CompactDisk::CompactDisk(std::string artist, std::string title, std::string mediaType, std::string shelfmark, bool available, std::string datePublished, bool deleted) : MediaItem(artist, title, mediaType, shelfmark, available, datePublished, deleted)
{
	this->setArtist(artist);
	this->m_NumTracks = 0;
	this->capacity = 5;
	this->m_Tracks = new Track[capacity];

	std::cout << "CD: 2nd constructor" << std::endl;
}

CompactDisk::~CompactDisk(){
	delete [] m_Tracks;
	std::cout << "CD destructor" << std::endl;
}

std::string CompactDisk::getArtist(){
	return getCreator();
}

void CompactDisk::setArtist(std::string artist){
	this->setCreator(artist);}

int CompactDisk::getNumberOfTracks(){
	return this->m_NumTracks;
}

void CompactDisk::setNumberOfTracks(int numTracks){
	this->m_NumTracks = numTracks;
}

Track * CompactDisk::getTracks(){
	return m_Tracks;
}

Track CompactDisk::getTrack(int index){
	if (index < 0 || index > m_NumTracks) {
		throw "Out of bounds exception";
	}
	return m_Tracks[index];
}

//void CompactDisk::addTrack(/*const*/ Track track){ // this still works 
//	if (m_NumTracks >= capacity) {
//		expand();
//	}
//	track.setLyrics("abcdefghijklmnopqrstuvwxyz"); // supposed to pass by value
//	m_Tracks[this->m_NumTracks++] = track;
//}



void CompactDisk::addTrack(const Track & track){
	if (m_NumTracks >= capacity) {
		expand();
	}
	m_Tracks[this->m_NumTracks++] = track;
}

void CompactDisk::expand() {
	std::cout << "CD array expansion";
	
	this->capacity *= 2;
	Track *temp = new Track[capacity];

	for (int i = 0; i < m_NumTracks; i++) {
		temp[i] = m_Tracks[i];
	}

	delete[] this->m_Tracks; // delete space made for this array

	m_Tracks = temp;
}

void CompactDisk::deleteTrack(int n)
{
	if (m_NumTracks <= 0) {
		throw "This cd has reached minimum capacity";
	}
	m_Tracks[this->m_NumTracks--];
}

std::string CompactDisk::toString()
{
	std::string tracks = "";
	for (int i = 0; i < m_NumTracks; i++) {
		tracks += std::to_string(i+1) + ": {"+ m_Tracks[i].toString() +"} ";

		//std::cout << m_Tracks[i].toString();

	}
	 return MediaItem::toString() + "\nArtist:\t\t" + getArtist() +
		"\n# of Tracks:\t" + std::to_string(m_NumTracks) +
		 "\nTracks:\t\t[" + tracks + "]";
}


std::string CompactDisk::toCSV()
{
	std::string tracks = "";
	for (int i = 0; i < m_NumTracks; i++) {
		tracks += std::to_string(i + 1) + ": {" + m_Tracks[i].toString() + "} ";

		//std::cout << m_Tracks[i].toString();

	}
	return MediaItem::toCSV() + "\nArtist:\t\t" + getArtist() +
		"\n# of Tracks:\t" + std::to_string(m_NumTracks) +
		"\nTracks:\t\t[" + tracks + "]";
}

//void CompactDisk::testPassByValue(Track track)
//{
//	track.setLyrics("PASSED BY VALUE SO RESULT SHOULDN'T CHANGE");
//}

int CDmain() {
//https://www.youtube.com/watch?v=jkGhKXhIM8U
	CompactDisk* album = new CompactDisk("Celine Dion", "The Cast Away", "CD", "11M4.44", true, "19-09-2000",false);

	Track* t = new Track();
	t->setTrackName("Sound of Music");
	t->setTrackLenth(30);
	t->setLyrics("The hills are alive with the sound of music\nWith songs they have sung for a thousand years\nThe hills fill my heart with the sound of music\nMy heart wants to sing every song it hears\nMy heart wants to beat like the wings of the birds\nThat rise from the lake to the trees\nMy heart wants to sigh like a chime that flies\nFrom a church on a breeze\nTo laugh like a brook when it trips and falls over\nStones on its way\nTo sing through the night like a lark who is learning to pray\nI go to the hills when my heart is lonely\nI know I will hear what I've heard before\nMy heart will be blessed with the sound of music\nAnd I�ll sing once more");
	//album[0].testPassByValue(*t);
	
	album->addTrack(*t);


	std::cout << "here--------------------\n\n\n\n" + t->toString() << std::endl;
	std::cout << album->toString() << std::endl;

	

	delete album;
	delete t;
	//delete t;
	
	std::cin.get();

	return 0;
}

//printf("c = %c, Capital letter 'A' = %i(in ASCII CODE), m = c*n = %i*%i =%i\n", c, c, c, n, m);
