#pragma once
#include <iostream>
#include "MediaItem.h"
#include "Track.h"

class CompactDisk : public MediaItem
{
private:
	//std::string m_Artist;
	int m_NumTracks;
	Track* m_Tracks;
	int capacity;
	void expand();

public:
	CompactDisk();
	CompactDisk(std::string artist, std::string title, std::string mediaType, std::string shelfmark, bool available, std::string datePublished, bool deleted);
	~CompactDisk();

	std::string getArtist();
	void setArtist(std::string artist);
	int getNumberOfTracks();
	void setNumberOfTracks(int numTracks);
	Track* getTracks();
	Track getTrack(int n);
	void addTrack(const Track & track);
	//void addTrack(/*const */Track  track);
	void deleteTrack(int n);

	std::string toString();
	std::string toCSV();

	//void testPassByValue(Track track);
};

