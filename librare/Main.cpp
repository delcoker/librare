#include "Book.h"
#include "CompactDisk.h"
#include "MediaItem.h"
#include <fstream>
#include <vector>
#include <algorithm>
#include <direct.h> 

//#include <memory> 




const string OPTION_BOOK = "1";
const string OPTION_CD = "2";
const string ADD = "3";
const string READ = "4";
const string UPDATE = "5";
const string DELETE = "6";
const string VIEW_ALL = "7";

const string BOOK = "book";
const string CD = "cd";
//const char dirname[10] = { 'db' };
const char* dirname = "db";
//const string dir = to_string(dirname);
const string str(dirname);
string book_dir;

const string BOOK_DB = "book_db.txt";
const string CD_DB = "cd_db.txt";


const string EXIT = "99";

const string LINE = "****************************************************";

const string intro(); const void outtro(); const void options(string, string); const string options_crud(string);
const vector<string> explode(const string& s, const char& c); 
const vector <shared_ptr<MediaItem>> find_media(vector <shared_ptr<MediaItem>> & medias, MediaItem &media_item, string media_item_type);
bool contains(const string & sentence, const string & str2);

vector <shared_ptr<MediaItem>> all_media;

bool book_deleted_or_updated = false;

int main() {

	bool continue_playing = true;

	int check;
	check = _mkdir(dirname);
	// check if directory is created or not
	if (!check);
		//cout << "Directory created";
	else {
		//cout << "Unable to create directory";
	}

	book_dir = "db/" + BOOK_DB;
	string cd_dir = "db/" + CD_DB;

	
	
	//vector<Book> all_books;
	//vector<CompactDisk> all_cds;

	ifstream b(book_dir);

	if (!b)
	{
		cerr << "Could not open file " << book_dir << endl;
		return 1;
	}
	
	// Read the entire book file into memory
	string s;
	string t;
	while (getline(b, t)) {

		s += t + '\n';
		// split the line by , and put in vector a
		vector<string> a{ explode(t, ',') };

		// push books into vector
		shared_ptr<MediaItem> book = make_shared <Book>(a[6], a[0], a[1], a[2], a[3] == "true", a[4], stoi(a[7]), a[5] == "true", a[8]);
		//cout << book->toString() << endl;
		if(!book->isDeleted())
			all_media.push_back(book);
		//delete book;

	
	}
	b.close();


	ifstream c(cd_dir);

	if (!c)
	{
		cerr << "Could not open file " << cd_dir << endl;
		//return 1;
	}

	// Read the entire  file into memory
	string u;
	string v;
	while (getline(c, v))
		u += v + '\n';
	c.close();


	
	/*testing find media in vector*/
	//check book exists 
	//source: https://stackoverflow.com/questions/571394/how-to-find-out-if-an-item-is-present-in-a-stdvector
	// 133,book,33,true,33333,3,3
	//Book bb = Book();
	//bb.setTitle("ti");
	//bb.setMediaType("book");
	//bb.setShelfmark("shelfmark");
	//bb.setAvailable(true);
	//bb.setDatePublished("date");
	//bb.setAuthour("author");
	//bb.setNumPages(9);

	//vector<MediaItem> found_media = find_media(all_media, bb, "all");



	do {
		//introduce the system
		string media = intro();

		if (media == EXIT) {
			continue_playing = false;
		}
		else if (media == BOOK || media == CD){

			// specify details about things in library


			//// if user key's in random data
			//while (intro_option != OPTION_BOOK && intro_option != OPTION_CD && intro_option != EXIT) {
			//	cout << "You entered a wrong option value. Please try again." << endl;
			//	cin >> intro_option;
			//}

			//if (media == OPTION_BOOK) {
			string crud = options_crud(media);
			options(media, crud);
		}
		//}
		//else if (media == OPTION_CD) {
		//	string crud = options_crud(media);
		//	options(media, crud);
		//}
		//else if (media == EXIT) {
		//	continue_playing = false;
		//}

		/*cin.clear();
		cin.ignore(10000, '\n');*/
	} while (continue_playing);




	outtro();
	//std::cin.get();
	return 0;
}

const void options(string media_item, string crud) {
	cin.clear();
	cin.ignore(10000, '\n');
	//add book
	if (media_item == BOOK) {
		string title, media_type = BOOK, shelfmark, date_published, text, authour; int num_pages = 1; bool deleted = false, available = true;

		if (crud == "add") {
			cout << "Please there the following details\n" << endl;
			cout << "\tTitle:\t\t";
			getline(cin, title);

			cout << "\tAuthour:\t";
			getline(cin, authour);

			cout << "\tShelfmark:\t";
			getline(cin, shelfmark);

			cout << "\tDate Published:\t";
			getline(cin, date_published);


			cout << "\tText:\t";
			getline(cin, text);

			cout << "\tNuber of pages:\t";
			cin >> num_pages;
			/*
			cout << "\tIs this book deleted:\t";
			cin >> deleted;
			*/

			// add a new book to the library
			shared_ptr<Book> new_book = make_shared<Book>(authour, title, media_type, shelfmark, available, date_published, num_pages, deleted, text);
			all_media.push_back(new_book);


			// DO NOT DO THIS write to file
			//Book* new_book = new Book(authour, title, media_type, shelfmark, available, date_published, num_pages, deleted, text);
			ofstream file;
			file.open(book_dir/**dirname + "/book_db.txt"*/, ios_base::app);
			file << new_book->toCSV() << endl;
			file.close();

			cout << "\tNew Book Added:\n" + LINE << endl << endl << new_book->toString() + "\n" + LINE << endl << endl;

		}
		else if (crud == "read") {
			string tsc;
			vector <shared_ptr <MediaItem>> found_media;
			cout << "Please enter part of or the Title, Shelfmark or Author/Artist of the " << media_item << endl << endl;
			std::getline(cin, tsc);

			shared_ptr<Book> bb = make_shared<Book>(tsc, tsc, BOOK, tsc, available, date_published, num_pages, deleted, text);
			found_media = find_media(all_media, *bb, BOOK);
			int i = 0;
			cout << "Found " + BOOK + "(s):" << endl;
			for (auto each_book : found_media) {
				cout << "\t" << i + 1 << ":\n" << LINE << endl << each_book->toString() << endl;
				i++;
			}

			// if it finds books
			// select the exact book you want and read further 
			if (found_media.size()) {
				cout << "Please enter number representing the exact " << media_item << " you want: " << endl << endl;
				getline(cin, tsc);
				if (stoi(tsc) <= found_media.size()) {
					shared_ptr<MediaItem> m = found_media[stoi(tsc) - 1];
					shared_ptr<Book> old_child = dynamic_pointer_cast<Book>(m);
					//  if cast was successful it should not be null
									/*
									if (old_child == nullptr) {
										cout << "NULL" << endl;
										cin.get();
										exit(0);
									}
									else
										cout << "NOT NULL" << endl;*/

					cout << "\t" << i << ":\n" << LINE << endl << old_child->toString() << endl;
					cout << endl << "The text in this " << media_item << " is: " << endl;
					cout << LINE << endl << old_child->getText() << endl;
					cout << LINE << endl << endl;

				}
				//delete m;
			}
		}
		else if (crud == "delete") { // delete from vector and db

			string tsc;
			vector <shared_ptr <MediaItem>> found_media;
			cout << "Please enter part of or the Title, Shelfmark or Author/Artist of the " << media_item << " you would like to delete." << endl << endl;
			std::getline(cin, tsc);

			shared_ptr<Book> bb = make_shared<Book>(tsc, tsc, BOOK, tsc, available, date_published, num_pages, deleted, text);
			found_media = find_media(all_media, *bb, BOOK);
			int i = 0;
			cout << "Found " + BOOK + "(s):" << endl;
			for (auto each_book : found_media) {
				cout << "\t" << i + 1 << ":\n" << LINE << endl << each_book->toString() << endl;
				i++;
			}

			// if it finds some books
			// select the exact book you want to delete
			if (found_media.size()) {
				cout << "Please enter number representing the exact " << media_item << " you would like to delete: " << endl << endl;
				getline(cin, tsc);
				if (stoi(tsc) <= found_media.size()) {

					for (auto item : all_media) {

						if (item->getShelfmark() == found_media[stoi(tsc) - 1]->getShelfmark()) {
							// delete it from vector
							auto position = std::find(all_media.begin(), all_media.end(), item); // shared_ptr <MediaItem> iterator
							all_media.erase(position);

							book_deleted_or_updated = true;
							break;
						}
					}

					cout << endl << "A " << media_item << " was deleted " << endl << endl;
					cout << LINE << endl << endl;
				}

				//if (book_deleted_or_updated) {

				//	//source: https://stackoverflow.com/questions/26576714/deleting-specific-line-from-file

				//	// write to new file
				//	ofstream temp; // contents of path must be copied to a temp file then renamed back to the path file
				//	temp.open("temp.txt");

				//	for (auto each_book : all_media) {
				//		shared_ptr<Book> old_child = dynamic_pointer_cast<Book>(each_book);
				//		temp << old_child->toCSV() << std::endl;
				//	}


				//	temp.close();

				//	const char * p = book_dir.c_str(); // required conversion for remove and rename functions
				//	remove(p);
				//	rename("temp.txt", p);

				//	book_deleted_or_updated = false;
				//}
			}

		}
		else if (crud == "update") {
			string tsc;
			vector <shared_ptr <MediaItem>> found_media;
			cout << "Please enter part of or the Title, Shelfmark or Author/Artist of the " << media_item << " you would like to edit." << endl << endl;
			std::getline(cin, tsc);

			shared_ptr<Book> bb = make_shared<Book>(tsc, tsc, BOOK, tsc, available, date_published, num_pages, deleted, text);
			found_media = find_media(all_media, *bb, BOOK);
			int i = 0;
			cout << "Found " + BOOK + "(s):" << endl;
			for (auto each_book : found_media) {
				cout << "\t" << i + 1 << ":\n" << LINE << endl << each_book->toString() << endl;
				i++;
			}

			// if it finds some books
			// select the exact book you want to delete
			if (found_media.size()) {
				cout << "Please enter number representing the exact " << media_item << " you would like to delete: " << endl << endl;
				getline(cin, tsc);
				if (stoi(tsc) <= found_media.size()) {

					for (auto item : all_media) {

						if (item->getShelfmark() == found_media[stoi(tsc) - 1]->getShelfmark()) {
							// edit it in vector

							cout << "Please there the following details\n" << endl;
							cout << "\tTitle:\t\t";
							getline(cin, title);
							item->setTitle(title);

							cout << "\tAuthour:\t";
							getline(cin, authour);

							cout << "\tShelfmark:\t";
							getline(cin, shelfmark);

							cout << "\tDate Published:\t";
							getline(cin, date_published);

							/*cout << "\tAvailable:\t";  // DOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO THIS ask user for true or false
							getline(cin, available);*/

							cout << "\tText:\t";
							getline(cin, text);

							cout << "\tNuber of pages:\t";
							cin >> num_pages;
							/*
							cout << "\tIs this book deleted:\t";
							cin >> deleted;
							*/

							// edit book library
							shared_ptr<Book> edited_book = make_shared<Book>(authour, title, media_type, shelfmark, available, date_published, num_pages, deleted, text);

							auto position = std::find(all_media.begin(), all_media.end(), item); // shared_ptr <MediaItem> iterator
							all_media.erase(position);
							all_media.push_back(edited_book);

							book_deleted_or_updated = true;
							break;
						}
					}

					cout << endl << "A " << media_item << " was edited " << endl << endl;
					cout << LINE << endl << endl;
				}


			}
		}
	}


	// if a book was deleted or updated, write to the file
	if (book_deleted_or_updated) {

		//source: https://stackoverflow.com/questions/26576714/deleting-specific-line-from-file

		// write to new file
		ofstream temp; // contents of path must be copied to a temp file then renamed back to the path file
		temp.open("temp.txt");

		for (auto each_book : all_media) {
			shared_ptr<Book> old_child = dynamic_pointer_cast<Book>(each_book);
			temp << old_child->toCSV() << std::endl;
		}


		temp.close();

		const char * p = book_dir.c_str(); // required conversion for remove and rename functions
		remove(p);
		rename("temp.txt", p);

		book_deleted_or_updated = false;
	}

}


//void option_crud_book(string media_item) {
//	
//}
//
//void option_crud_cd(string media_item) {
//	// add a new cd to the library
//
//}

const string options_crud(string media_item) {
	cout << "Please pick an action to perform" << endl;
	cout << "Enter:\n\t3. To add a new " << media_item << "\n\t4. To search for a "  << media_item << endl;
	cout << "\t5. To edit a " << media_item << "\n\t6. To delete a " << media_item << endl;
	cout << "\t7. To view all " << media_item << "s." << endl;
	string crud_selected;
	cin >> crud_selected;

	if (crud_selected == ADD) {
		return "add";
	}
	else if (crud_selected == READ) {
		return "read";
	}
	else if (crud_selected == UPDATE) {
		return "update";
	}
	else if (crud_selected == DELETE) {
		return "delete";
	}
	else if (crud_selected == VIEW_ALL) {
		return "view";
	}
	return "";
}

const string intro() {
	cout << "Welcome to the Loughborough Bibliographic Mangagement System" << endl;
	cout << "Select:\n\t1. For Book Options\n\t2. For CD Options" << endl;
	cout << "\n\t99.To Exit" << endl;
	string media_selected;
	cin >> media_selected;

	if (media_selected == OPTION_BOOK) {
		media_selected = "book";
	}
	else if (media_selected == OPTION_CD) {
		media_selected = "cd";
	}
	else if (media_selected == EXIT) {
		media_selected = "99";
	}

	cin.clear();
	cin.ignore(10000, '\n');
	return media_selected;
}

const void outtro() {
	cout << "Thank you for using the Loughborough Bibliographic Mangagement System" << endl;
	cout << "Hit enter any key to close this window..." << endl;
}

//split a string by specified character
//source: http://www.cplusplus.com/articles/2wA0RXSz/
const vector<string> explode(const string& s, const char& c)
{
	string buff{ "" };
	vector<string> v;

	for (auto n : s)
	{
		if (n != c) buff += n; else
			if (n == c && buff != "") { v.push_back(buff); buff = ""; }
	}
	if (buff != "") v.push_back(buff);

	return v;
}

// Search for a Media by shelfmark or title
const vector <shared_ptr<MediaItem>> find_media(vector <shared_ptr<MediaItem>> & medias, MediaItem &media_item, string media_item_type) {
	vector < shared_ptr<MediaItem>> found;

	for (auto one : medias) {
		if (media_item_type == "all") {

			if ((contains(one->getCreator(), media_item.getCreator()) ||
				contains(one->getShelfmark(), media_item.getShelfmark())
				|| contains(one->getTitle(), media_item.getTitle())))
			{
				//cout << "\n-----match---no type-- books and cds------" /*<< one.getAuthour()*/ << endl;
				//cout << "-----match-----------" << endl;

				found.push_back(one);
			}
		}
		else {
			if ((contains(one->getCreator(), media_item.getCreator()) ||
				contains(one->getShelfmark(), media_item.getShelfmark())
				|| contains(one->getTitle(), media_item.getTitle()))
				&& contains(one->getMediaType(), media_item_type))
			{
				//cout << "\n-----match-----------" /*<< one.getAuthour()*/ << endl;
				//cout << "-----match-----------" << endl;

				found.push_back(one);
			}
		}
	}
	return found;
}

// Search for a biij by shelfmark or title
//const vector<Book> find_book(vector<Book> & books, Book & book) {
//	vector<Book> found;
//
//	for (auto one : books) {
//		if (contains(one.getAuthour(), book.getAuthour()) || 
//			contains(one.getShelfmark(), book.getShelfmark())
//			|| contains(one.getTitle(), book.getTitle()))
//		{
//			cout << "\n-----match-----------" /*<< one.getAuthour()*/ << endl;
//			//cout << "-----match-----------" << endl;
//
//			found.push_back(one);
//		}
//	}
//	return found;
//}

//check if a string contains another
//source: https://stackoverflow.com/questions/2340281/check-if-a-string-contains-a-string-in-c
bool contains(const string & phrase, const string & str2) {

	string phrase2 = phrase;
	string str22 = str2;

	transform(phrase2.begin(), phrase2.end(), phrase2.begin(), ::tolower);
	transform(str22.begin(), str22.end(), str22.begin(), ::tolower);


	return (phrase2.find(str22) != string::npos);
		/*
		string str("the needles in a haystack");
		string str2("needle");

		if (str.find(str2) != string::npos) {
			cout << str2 << " found!" << '\n';
			cout << str.find(str2) << '\n';
		}
		*/
}

//bool to_bool(string str) {
//	if (str == "true")
//		return true;
//}
/*
student_id
Name
Age

*/
/*
checkout
date_checkout
date_to_return
date_returnedd\

*/


/*
MongoDB

Table	- Collection	: large
Row		- Document		: small

In the tweet document put person_id


Use composit key as id in bridge table

*/