#pragma once
#include "MediaItem.h"
//#include <stdio.h> // this is a c library file
//#include <iostream>

using namespace std;

MediaItem::MediaItem()
{
	m_Creator = "No Creator";
	m_Title = "No Title";
	m_Media_type = "No Media Type";
	m_Shelfmark = "No Shelfmark";
	m_Available = false;
	m_Date_Published = "01-01-1999";
	m_Deleted = false;
	//m_Date_created = "01-01-1999";
	//m_Date_modified = "01-01-1999";
}

MediaItem::MediaItem(string creator, string title, string mediaType, string shelfmark, bool available, string datePublished, bool deleted)
{
	m_Creator = creator;
	m_Title = title;
	m_Media_type = mediaType;
	m_Shelfmark = shelfmark;
	m_Available = available;
	m_Date_Published = datePublished;
	m_Deleted = deleted;
	//m_Date_created = DateCreated;
	//date_modified = DateModified;
}



MediaItem::~MediaItem()
{
}

//void MediaItem::print()
//{
//	"Making Polymorphic";
//}

string MediaItem::getTitle()
{
	return this->m_Title;
}

void MediaItem::setTitle(string title)
{
	this->m_Title = title;
}

string MediaItem::getMediaType() const
{
	return this->m_Media_type;
}

void MediaItem::setMediaType(string mediaType)
{
	m_Media_type = mediaType;
}

string MediaItem::getShelfmark() const
{
	return m_Shelfmark;
}

void MediaItem::setShelfmark(string shelfmark)
{
	m_Shelfmark = shelfmark;
}

string MediaItem::getDatePublished() const {
	return m_Date_Published;
}

void MediaItem::setDatePublished(string datePublished){
	m_Date_Published = datePublished;
}

std::string MediaItem::getCreator()
{
	return m_Creator;
}

void MediaItem::setCreator(std::string creator)
{
	m_Creator = creator;
}

bool MediaItem::isDeleted() const
{
	return m_Deleted;
}

void MediaItem::setDeleted(bool deleted)
{
	this->m_Deleted = deleted;
}

bool MediaItem::getAvailable() const {
	return m_Available;
}

void MediaItem::setAvailable(bool available) {
	m_Available = available;
}

string MediaItem::toString()
{
	return "Title:\t\t" + m_Title + "\nMedia Type:\t" + m_Media_type +
		"\nShelfmark:\t" + m_Shelfmark + "\nAvalaible:\t" + (m_Available ? "true" : "false")
		+ "\nDate Published:\t" + m_Date_Published+ "\nDeleted:\t" + (m_Deleted ? "true" : "false");
}

string MediaItem::toCSV()
{
	return m_Title + "," + m_Media_type +
		"," + m_Shelfmark + "," + (m_Available ? "true" : "false") + "," + m_Date_Published + "," + (m_Deleted ? "true" : "false");
}