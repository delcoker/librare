#pragma once
//#include <stdio.h> // this is a c library file
//#include <iostream>
#include <string>

//using namespace std;	// so you don't have to type std all the time

class MediaItem {
private:
	//int m_Item_id;
	std::string m_Title;
	std::string m_Media_type;
	std::string m_Shelfmark; // id
	bool m_Available;
	std::string m_Date_Published;
	std::string m_Creator; //author//artist
	bool m_Deleted;
	//std::string m_Date_Created;
	//std::string m_Date_Modified;

public:
	MediaItem();
	MediaItem(std::string creator, std::string title, std::string mediaType, std::string shelfmark, bool available, std::string datePublished, bool deleted);
	virtual ~MediaItem(); // to make this class polymorphic

	std::string getTitle();
	void setTitle(std::string title);
	std::string getMediaType() const;		// const because we don't want to change value in method definition?
	void setMediaType(std::string mediaType);
	std::string getShelfmark() const;
	void setShelfmark(std::string shelfmark);
	bool getAvailable() const;
	void setAvailable(bool available);
	std::string getDatePublished() const;
	void setDatePublished(std::string datePublished);
	std::string getCreator();
	void setCreator(std::string creator);

	bool isDeleted() const;
	void setDeleted(bool deleted);

	std::string toString();
	std::string toCSV();
};