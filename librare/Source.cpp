#include <iostream>
using namespace std;

class Base {
	virtual void print() { cout << "C++ be guy" << endl; }
};

class Derived1: public Base {
	void print() { cout << "Devrived1" << endl; }
};

class Derived2: public Base {
	void print() { cout << "Devrived2" << endl; }
};


int mainDynamicCasting() {
	Derived1 d1;

	Base *bp = dynamic_cast<Base*>(&d1);
	Derived2 * dp2 = dynamic_cast<Derived2*>(bp);
	
	if (dp2 == nullptr) 
		cout << "NULL" << endl;
	else
		cout << "NOT NULL" << endl;

	cin.get();
	return 0;
}
