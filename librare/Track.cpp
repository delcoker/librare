#include "Track.h"



Track::Track(){
	m_Lyrics = "No Lyrics";
	m_TrackName = "No Track Name";
	m_TrackLength = 0;

	std::cout << "Track default constructor" << std::endl;
}

Track::Track(std::string name, std::string lyrics, int length){
	m_Lyrics = lyrics;
	m_TrackName = name;
	m_TrackLength = length;

	std::cout << "Track default constructor" << std::endl;
}

Track::~Track()
{
	std::cout << "Track destructor" << std::endl;
}

std::string Track::getLyrics()
{
	return this->m_Lyrics;
}

void Track::setLyrics(std::string lyrics)
{
	this->m_Lyrics = lyrics;
}

std::string Track::getTrackName()
{
	return this->m_TrackName;
}

void Track::setTrackName(std::string trackName)
{
	this->m_TrackName = trackName;
}

int Track::getTrackLegth()
{
	return this->m_TrackLength;
}

void Track::setTrackLenth(int length)
{
	this->m_TrackLength = length;
}

std::string Track::toString()
{
	return "Track: " + m_TrackName + "\tLength: " + std::to_string(m_TrackLength) + "\tLyrics: " + m_Lyrics + "";
}

int mainTrackTest() {
	Track * t = new Track();
	t->setTrackName("HEYEYEYE");
	t->setTrackLenth(20);
	t->setLyrics("HEYEYEdafadfadsfadsfadsffffffffffffffffffffffffffffffffffffffsssssssssssssssssssssYE");

	std::cout << t->toString();


	delete t;

	std::cin.get();
	
	return 0;
}
