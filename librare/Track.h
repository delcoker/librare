#pragma once
//#include <stdio.h>
#include <iostream>
#include <string>


class Track
{
private:
	std::string m_Lyrics;
	std::string m_TrackName;
	int m_TrackLength;
	//int m_TrackNumber;

public:
	Track();
	Track(std::string name, std::string lyrics, int length);
	~Track();

	std::string getLyrics();
	void setLyrics(std::string lyrics);
	std::string getTrackName();
	void setTrackName(std::string trackName);
	int getTrackLegth();
	void setTrackLenth(int length);
	std::string toString();
};
